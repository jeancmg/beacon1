package pe.com.co_de.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import pe.com.co_de.MainActivity;
import pe.com.co_de.R;
import pe.com.co_de.SQLite.AdminSQLite;
import pe.com.co_de.Singleton.Singleton;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        startHeavyProcessing();
    }
    private void startHeavyProcessing(){
        new LongOperation().execute("");
    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SQLiteDatabase db = null;
            try {
                Thread.sleep(1000);
                AdminSQLite admin = new AdminSQLite(SplashActivity.this, "datos", null, 1);
                db = admin.getWritableDatabase();
                String[] col = new String[]{"token"};
                Cursor fi = db.query("usuarios", col, null, null, null, null, null);
                if (fi != null) {
                    if (fi.moveToNext()) {
                        Singleton.getInstance().token = fi.getString(0);
                    }
                }
            } catch (InterruptedException e) {
                Thread.interrupted();
            } finally {
                db.close();
            }
            return "ok";//"noInternet";
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("token", Singleton.getInstance().token);
            if (Singleton.getInstance().token.equals("") || Singleton.getInstance().token.isEmpty()) {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            } else {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            }
            finish();
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
