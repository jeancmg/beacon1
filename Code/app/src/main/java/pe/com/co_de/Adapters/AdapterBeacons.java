package pe.com.co_de.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import pe.com.co_de.ImageRequest.ImageRequestQueue;
import pe.com.co_de.MainActivity;
import pe.com.co_de.Objetos.Beacons;
import pe.com.co_de.R;

public class AdapterBeacons extends RecyclerView.Adapter<AdapterBeacons.viewHolder>{

    List<Beacons> beacons;
    private static Context context;
    private ImageLoader mImageLoader;

    public AdapterBeacons(Context context, List<Beacons> beacons) {
        this.context = context;
        this.beacons = beacons;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_beacon, parent, false);
        viewHolder holder = new viewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final viewHolder holder, int position) {
        Beacons p = beacons.get(position);
        holder.id.setText(String.valueOf(p.getId()));

        mImageLoader = ImageRequestQueue.getInstance(context)
                .getImageLoader();
        mImageLoader.get(context.getString(R.string.server) + "/img/" + p.getId() + ".jpg", ImageLoader.getImageListener(holder.imagen,
                R.drawable.preview, android.R.drawable
                        .ic_dialog_alert));
        holder.imagen.setImageUrl(context.getString(R.string.server) + "/img/" + p.getId() + ".jpg", mImageLoader);

        holder.titulo.setText(p.getTitulo());
        holder.descripcion.setText(p.getDescripcion());
        holder.distancia.setText(p.getDistancia());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(holder.id.getText().toString().substring(holder.id.getText().toString().indexOf("|")+1, holder.id.getText().toString().length())));
                context.startActivity(browserIntent);*/
                if (context instanceof MainActivity) {
                    ((MainActivity)context).openDialog(holder.id.getText().toString());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return beacons.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        NetworkImageView imagen;
        TextView id, titulo, descripcion, distancia;

        public viewHolder(final View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            id = itemView.findViewById(R.id.id);
            imagen = itemView.findViewById(R.id.imagen);
            titulo = itemView.findViewById(R.id.titulo);
            descripcion = itemView.findViewById(R.id.descripcion);
            distancia = itemView.findViewById(R.id.distancia);
        }
    }

}
